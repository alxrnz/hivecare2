from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

class Beekeeping(models.Model):
    name = models.CharField(max_length=50, unique=True)
    description = models.CharField(max_length=100)

    def __str__(self):
        return self.name

class Apiarist(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    beekeeping = models.ForeignKey(Beekeeping, related_name='apiarists', on_delete=models.CASCADE, null=True)

@receiver(post_save, sender=User)
def create_user_apiarist(sender, instance, created, **kwargs):
    if created:
        Apiarist.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_apiarist(sender, instance, **kwargs):
    instance.apiarist.save()


class Location(models.Model):
    name = models.CharField(max_length=50) 
    city = models.CharField(max_length=50)
    street = models.CharField(max_length=50)
    description = models.CharField(max_length=100)
    beekeeping = models.ForeignKey(Beekeeping, related_name='locations', on_delete=models.CASCADE)
    
    def __str__(self):
        return self.name


class Hive(models.Model):
    #internal enums
    HIVETYPES = (
        ('-----', '-----'),
        ('Jungvolk', 'Jungvolk'),
        ('Wirtschaftsvolk', 'Wirtschaftsvolk'),
        ('Ableger', 'Ableger'),
        ('Schwarm', 'Schwarm'),
        ('Aufgelöst', 'Aufgelöst'),
        )
    FLIGHTHOLEWEDGE = (
        ('-----', '-----'),
        ('zu', 'zu'),
        ('kleine Öffnung', 'kleine Öffnung'),
        ('große Öffnung', 'große Öffnung'),
        ('ohne', 'ohne'),
        )
    TRUE_FALSE = (
        (True, 'Ja'),
        (False, 'Nein'),
        )
    name = models.CharField(max_length=50, unique=True)
    hiveType = models.CharField(max_length=100, choices=HIVETYPES, default='-----')
    beeQueenNr = models.IntegerField(default=0)
    beeQueenBirthDate = models.IntegerField(default=0)
    beeQueenMarked = models.BooleanField(default=False, choices=TRUE_FALSE)
    gentleness = models.IntegerField(default=0)
    honeyCombFaithful = models.IntegerField(default=0)
    swarmInertia = models.IntegerField(default=0)
    stingingLoss = models.IntegerField(default=0)
    droneFrame = models.IntegerField(default=0, help_text='+/-')
    brutFrame = models.IntegerField(default=0, help_text='+/-')
    emptyFrame = models.IntegerField(default=0, help_text='+/-')
    feedFrame = models.IntegerField(default=0, help_text='+/-')
    mediumWallFrame = models.IntegerField(default=0, help_text='+/-')
    beesEscape = models.BooleanField(default=False, choices=TRUE_FALSE)
    flightHoleWedge = models.CharField(max_length=20, choices=FLIGHTHOLEWEDGE, default='-----')
    honeyFrame = models.IntegerField(default=0, help_text='+/-')
    emptyBox = models.IntegerField(default=0, help_text='+/-')
    fence = models.BooleanField(default=False, choices=TRUE_FALSE)
    diaper = models.BooleanField(default=False, choices=TRUE_FALSE)
    notes = models.TextField(max_length=500)    
    location = models.ForeignKey(Location, related_name='hives', on_delete=models.CASCADE)
    beekeeping = models.ForeignKey(Beekeeping, related_name='all_hives', on_delete=models.CASCADE)
    
    def __str__(self):
        return self.name

class Observation(models.Model):
    #internal enums
    FEEDHONEYCOMB = (
        ('NOP', '-----'),
        ('VI','viel'),
        ('WE','wenig'),
        ('KE','kein'),
        )
    TRAFFIC = (
        ('NOP', '-----'),
        ('VI','viel'),
        ('WE','wenig'),
        ('KE','kein'),
        )
    TRUE_FALSE = (
        (True, 'Ja'),
        (False, 'Nein'),
        )
    name = models.CharField(max_length=20, default="Beobachtung")
    queenLessCell = models.BooleanField(default=False, choices=TRUE_FALSE)    
    gameCup = models.BooleanField(default=False, choices=TRUE_FALSE)
    feedHoneyComb = models.CharField(max_length=20, choices=FEEDHONEYCOMB, default='-----')
    queen = models.BooleanField(default=False, choices=TRUE_FALSE)
    brood = models.BooleanField(default=False, choices=TRUE_FALSE)
    pins = models.BooleanField(default=False, choices=TRUE_FALSE)
    varroa = models.BooleanField(default=False, choices=TRUE_FALSE)
    pollenEntry = models.BooleanField(default=False, choices=TRUE_FALSE)
    traffic = models.CharField(max_length=20, choices=TRAFFIC, default='-----')
    temperature = models.FloatField(default=0.00, help_text='°C')
    weather = models.IntegerField(default=0)
    miteFall = models.IntegerField(default=0)
    weight = models.FloatField(default=0.00, help_text='in kg')
    notes = models.TextField(max_length=500)
    hive = models.ForeignKey(Hive, related_name='observations', on_delete=models.CASCADE)
    executed_at = models.DateTimeField(auto_now_add=True)
    executed_by = models.ForeignKey(User, null=True, related_name='observations', on_delete=models.CASCADE)
    
    def __int__(self):
        return self.pk

class LaborProcess(models.Model):
    #internal enums
    FLIGHTHOLEWEDGE = (
        ('NOP', '-----'),
        ('zu', 'zu'),
        ('kleine Öffnung', 'kleine Öffnung'),
        ('große Öffnung', 'große Öffnung'),
        ('ohne', 'ohne'),
        )
    TRUE_FALSE = (
        (True, 'Ja'),
        (False, 'Nein'),
        )        
    name = models.CharField(max_length=20, default="Arbeitsvorgang")
    droneFrame = models.IntegerField(default=0, help_text='+/-')
    brutFrame = models.IntegerField(default=0, help_text='+/-')
    emptyFrame = models.IntegerField(default=0, help_text='+/-')
    feedFrame = models.IntegerField(default=0, help_text='+/-')
    mediumWallFrame = models.IntegerField(default=0, help_text='+/-')
    honeyFrame = models.IntegerField(default=0, help_text='+/-')
    emptyBox = models.IntegerField(default=0, help_text='+/-')
    beesEscape = models.BooleanField(default=False, choices=TRUE_FALSE)
    fence = models.BooleanField(default=False, choices=TRUE_FALSE)
    diaper = models.BooleanField(default=False, choices=TRUE_FALSE)
    flightHoleWedge = models.CharField(max_length=20, choices=FLIGHTHOLEWEDGE, default='-----')
    notes = models.TextField(max_length=500)   
    hive = models.ForeignKey(Hive, related_name='laborprocesses', on_delete=models.CASCADE)
    executed_at = models.DateTimeField(auto_now_add=True)
    executed_by = models.ForeignKey(User, null=True, related_name='laborprocesses', on_delete=models.CASCADE)
    
    def __int__(self):
        return self.pk
    
class Treatment(models.Model):
    #internal enums
    ACIDTYPE = (
        ('AM', 'Ameisensäure'),
        ('OX', 'Oxalsäure'),
        ('MI', 'Milchsäure'),
        )
    TRUE_FALSE = (
        (True, 'Ja'),
        (False, 'Nein'),
        )  
    name = models.CharField(default='Behandlung', max_length=20)
    acidType = models.CharField(max_length=20, choices=ACIDTYPE)
    ammount = models.FloatField(default=0, help_text='in ml')
    brood = models.BooleanField(default=False, choices=TRUE_FALSE)
    beePin = models.BooleanField(default=False, choices=TRUE_FALSE)
    varroa = models.BooleanField(default=False, choices=TRUE_FALSE)
    weight = models.FloatField(default=0, help_text='in kg')
    notes = models.TextField(max_length=500)
    hive = models.ForeignKey(Hive, related_name='treatments', on_delete=models.CASCADE)
    executed_at = models.DateTimeField(auto_now_add=True)
    executed_by = models.ForeignKey(User, null=True, related_name='treatments', on_delete=models.CASCADE)

    def __int__(self):
        return self.pk

class Feeding(models.Model):
    #internal enums
    FEEDTYPE = (
        ('ZU', 'Zuckerwasser'),
        ('SI', 'Sirup'),
        ('FU', 'Futterteig'),
        )
    name = models.CharField(default='Fütterung', max_length=20)
    feedType = models.CharField(max_length=20, choices=FEEDTYPE)
    ammount = models.FloatField(default=0, help_text='in kg/l')
    notes = models.TextField(max_length=500)
    hive = models.ForeignKey(Hive, related_name='feedings', on_delete=models.CASCADE)
    executed_at = models.DateTimeField(auto_now_add=True)
    executed_by = models.ForeignKey(User, null=True, related_name='feedings', on_delete=models.CASCADE)

    def __int__(self):
        return self.pk

class Harvest(models.Model):
    name = models.CharField(default='Ernte', max_length=20)
    honeyComb = models.IntegerField(default=0)
    honeyType = models.CharField(max_length=100, default="Blütenhonig")
    ammount = models.FloatField(default=0, help_text='in kg')
    waterValue = models.IntegerField(default=0, help_text='%')
    notes = models.TextField(max_length=500)
    hive = models.ForeignKey(Hive, related_name='harvests', on_delete=models.CASCADE)
    executed_at = models.DateTimeField(auto_now_add=True)
    executed_by = models.ForeignKey(User, null=True, related_name='harvests', on_delete=models.CASCADE)

    def __int__(self):
        return self.pk

class NewsFeed(models.Model):
    action_type = models.CharField(max_length=15)
    action_id = models.IntegerField(default=0)
    executed_at = models.DateTimeField(auto_now_add=True)
    executed_by = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    notes = models.TextField(max_length=500)
    hive = models.ForeignKey(Hive, on_delete=models.CASCADE)
    beekeeping = models.ForeignKey(Beekeeping, related_name='actions', on_delete=models.CASCADE)

    def __int__(self):
        return self.pk