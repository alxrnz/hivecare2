from django import forms
from .models import Location, Hive, Observation, LaborProcess, Treatment, Feeding, Harvest

class NewLocationForm(forms.ModelForm):
    Location.description = forms.CharField(label='Beschreibung',
        widget=forms.Textarea(
            attrs={'rows': 5, 'placeholder': 'Genauere Beschreibung hier einfügen!'}
        ),
        max_length=200,
        help_text='Die maximale Anzahl Zeichen sind 200'
    )

    class Meta:
        model = Location
        labels = {'name': 'Name',
                  'street': 'Straße',
                  'city': 'Stadt',
                  'description': 'Beschreibung',
                  }
        fields = ['name',
                  'street',
                  'city',
                  'description',
                  ]

class NewHiveForm(forms.ModelForm):

    notes = forms.CharField(label='Notiz', required=False,
        widget=forms.Textarea(
            attrs={'rows': 5, 'placeholder': 'Genauere Beschreibung hier einfügen!'}
        ),
        max_length=200,
        help_text='Die maximale Anzahl Zeichen sind 200'
    )
    class Meta:
        model = Hive
        labels = {'name': 'Name',
                  'hiveType': 'Art',
                  'beeQueenNr': 'Königin Nr.',
                  'beeQueenBirthDate': 'Königin Geburtsjahr',
                  'beeQueenMarked': 'Königin markiert',
                  'gentleness': 'Sanftmut',
                  'honeyCombFaithful': 'Wabentreue',
                  'swarmInertia': 'Schwarmträgheit',
                  'stingingLoss': 'Stechlust',
                  'location': 'Standort',
                  'notes': 'Notizen',
            }
        fields = ['name',
                  'hiveType',
                  'beeQueenNr',
                  'beeQueenBirthDate',
                  'beeQueenMarked',
                  'gentleness',
                  'honeyCombFaithful',
                  'swarmInertia',
                  'stingingLoss',
                  'location',
                  'notes',
                  ]

class NewObservationForm(forms.ModelForm):

    notes = forms.CharField(label='Notiz', required=False, 
        widget=forms.Textarea(
            attrs={'rows': 5, 'placeholder': 'Genauere Beschreibung hier einfügen!'}
        ),
        max_length=200,
        help_text='Die maximale Anzahl Zeichen sind 200'
    )
    class Meta:
        model = Observation

        labels = {'queenLessCell': 'Weiselzellen vorhanden',
                  'gameCup': 'Spielnäpfchen vorhanden',
                  'feedHoneyComb': 'Futterwaben vorhanden',
                  'queen': 'Königin vorhanden',
                  'brood': 'Brut vorhanden',
                  'pins': 'Stifte vorhanden',
                  'varroa': 'Varroa Befall',
                  'pollenEntry': 'Polleneintrag vorhanden',
                  'traffic': 'Flugverkehr',
                  'temperature': 'Temperatur',
                  'weather': 'Wetter',
                  'miteFall': 'Milbenfall',
                  'weight': 'Gewicht Stock',
                  'notes': 'Notizen',
            }

        fields = ['queenLessCell',
                  'gameCup',
                  'feedHoneyComb',
                  'queen',
                  'brood',
                  'pins',
                  'varroa',
                  'pollenEntry',
                  'traffic',
                  'temperature',
                  'weather',
                  'miteFall',
                  'weight',
                  'notes',
                  ]

class NewLaborProcessForm(forms.ModelForm):

    notes = forms.CharField(label='Notiz', required=False,
        widget=forms.Textarea(
            attrs={'rows': 5, 'placeholder': 'Genauere Beschreibung hier einfügen!'}
        ),
        max_length=200,
        help_text='Die maximale Anzahl Zeichen sind 200'
    )
    class Meta:
        model = LaborProcess
        labels = {'droneFrame': 'Drohenrahmen',
                  'brutFrame': 'Brutrahmen',
                  'emptyFrame': 'Leerrahmen',
                  'feedFrame': 'Futterrahmen',
                  'mediumWallFrame': 'Mittelrandrahmen',
                  'honeyFrame': 'Honigzarge',
                  'emptyBox': 'Leerzarge',
                  'beesEscape': 'Bienenflucht',
                  'fence': 'Absperrgitter',
                  'diaper': 'Windel',
                  'flightHoleWedge': 'Fluglochkeil',
                  'notes': 'Notizen',
            }
        fields = ['droneFrame',
                  'brutFrame',
                  'emptyFrame',
                  'feedFrame',
                  'mediumWallFrame',
                  'honeyFrame',
                  'emptyBox',
                  'beesEscape',
                  'fence',
                  'diaper',
                  'flightHoleWedge',
                  'notes',
                  ]

class NewTreatmentForm(forms.ModelForm):

    notes = forms.CharField(label='Notiz', required=False,
        widget=forms.Textarea(
            attrs={'rows': 5, 'placeholder': 'Genauere Beschreibung hier einfügen!'}
        ),
        max_length=200,
        help_text='Die maximale Anzahl Zeichen sind 200'
    )
    class Meta:
        model = Treatment
        labels = {'acidType': 'Art des Mittels',
                  'ammount': 'Menge',
                  'brood': 'Brut behandelt',
                  'beePin': 'Stifte behandelt',
                  'varroa': 'Varroa behandelt',
                  'weight': 'Gewicht',
                  'notes': 'Notizen',
            }
        fields = ['acidType',
                  'ammount',
                  'brood',
                  'beePin',
                  'varroa',
                  'weight',
                  'notes',
                  ]

class NewFeedingForm(forms.ModelForm):

    notes = forms.CharField(label='Notiz', required=False,
        widget=forms.Textarea(
            attrs={'rows': 5, 'placeholder': 'Genauere Beschreibung hier einfügen!'}
        ),
        max_length=200,
        help_text='Die maximale Anzahl Zeichen sind 200'
    )
    class Meta:
        model = Feeding
        labels = {'feedType': 'Art des Futters',
                  'ammount': 'Menge',
                  'notes': 'Notizen',
            }
        fields = ['feedType',
                  'ammount',
                  'notes',
                  ]

class NewHarvestForm(forms.ModelForm):

    notes = forms.CharField(label='Notiz', required=False,
        widget=forms.Textarea(
            attrs={'rows': 5, 'placeholder': 'Genauere Beschreibung hier einfügen!'}
        ),
        max_length=200,
        help_text='Die maximale Anzahl Zeichen sind 200'
    )
    class Meta:
        model = Harvest
        labels = {'honeyComb': 'Waben',
                  'honeyType': 'Art des Honigs',
                  'ammount': 'Menge',
                  'waterValue': 'Wassergehalt',
                  'notes': 'Notizen',
            }
        fields = ['honeyComb',
                  'honeyType',
                  'ammount',
                  'waterValue',
                  'notes',
                  ]

class EditHiveForm(forms.ModelForm):

    class Meta:
        model = Hive
        labels = {'hiveType': 'Art',
                  'beeQueenNr': 'Königin Nr.',
                  'beeQueenBirthDate': 'Königin Geburtsjahr',
                  'beeQueenMarked': 'Königin markiert',
                  'gentleness': 'Sanftmut',
                  'honeyCombFaithful': 'Wabentreue',
                  'swarmInertia': 'Schwarmträgheit',
                  'stingingLoss': 'Stechlust',
                  'location': 'Standort',
                  'notes': 'Notizen',
            }
        fields = ['hiveType',
                  'beeQueenNr',
                  'beeQueenBirthDate',
                  'beeQueenMarked',
                  'gentleness',
                  'honeyCombFaithful',
                  'swarmInertia',
                  'stingingLoss',
                  'location',
                  'notes',
                  ]

class EditHiveNameForm(forms.ModelForm):

    class Meta:
        model = Hive
        labels = {'name': 'Name'}
        fields = ['name']

class ObservationForm(forms.ModelForm):

    class Meta:
        model = Observation

        labels = {'queenLessCell': 'Weiselzelle',
                  'gameCup': 'Spielnäpfchen',
                  'feedHoneyComb': 'Futterwabe',
                  'queen': 'Königin',
                  'brood': 'Brut',
                  'pins': 'Stifte',
                  'varroa': 'Varroa',
                  'pollenEntry': 'Polleneintrag',
                  'traffic': 'Flugverkehr',
                  'temperature': 'Temperatur',
                  'weather': 'Wetter',
                  'miteFall': 'Milbenfall',
                  'weight': 'Gewicht',
                  'notes': 'Notizen',
            }

        fields = ['queenLessCell',
                  'gameCup',
                  'feedHoneyComb',
                  'queen',
                  'brood',
                  'pins',
                  'varroa',
                  'pollenEntry',
                  'traffic',
                  'temperature',
                  'weather',
                  'miteFall',
                  'weight',
                  'notes',
                  ]

class LaborProcessForm(forms.ModelForm):

    class Meta:
        model = LaborProcess

        labels = {'droneFrame': 'Drohenrahmen',
                  'brutFrame': 'Brutrahmen',
                  'emptyFrame': 'Leerrahmen',
                  'feedFrame': 'Futterrahmen',
                  'mediumWallFrame': 'Mittelrandrahmen',
                  'honeyFrame': 'Honigzarge',
                  'emptyBox': 'Leerzarge',
                  'beesEscape': 'Bienenflucht',
                  'fence': 'Absperrgitter',
                  'diaper': 'Windel',
                  'flightHoleWedge': 'Fluglochkeil',
                  'notes': 'Notizen',
            }
        fields = ['droneFrame',
                  'brutFrame',
                  'emptyFrame',
                  'feedFrame',
                  'mediumWallFrame',
                  'honeyFrame',
                  'emptyBox',
                  'beesEscape',
                  'fence',
                  'diaper',
                  'flightHoleWedge',
                  'notes',
                  ]

class TreatmentForm(forms.ModelForm):

    class Meta:
        model = Treatment
        labels = {'acidType': 'Art des Mittels',
                  'ammount': 'Menge',
                  'brood': 'Brut',
                  'beePin': 'Stifte',
                  'varroa': 'Varroa',
                  'weight': 'Gewicht',
                  'notes': 'Notizen',
            }
        fields = ['acidType',
                  'ammount',
                  'brood',
                  'beePin',
                  'varroa',
                  'weight',
                  'notes',
                  ]

class FeedingForm(forms.ModelForm):

    class Meta:
        model = Feeding
        labels = {'feedType': 'Art des Futters',
                  'ammount': 'Menge',
                  'notes': 'Notizen',
            }
        fields = ['feedType',
                  'ammount',
                  'notes',
                  ]

class HarvestForm(forms.ModelForm):

    class Meta:
        model = Harvest
        labels = {'honeyComb': 'Waben',
                  'honeyType': 'Art des Honigs',
                  'ammount': 'Menge',
                  'waterValue': 'Wassergehalt',
                  'notes': 'Notizen',
            }
        fields = ['honeyComb',
                  'honeyType',
                  'ammount',
                  'waterValue',
                  'notes',
                  ]
