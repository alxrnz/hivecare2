from django.contrib import admin
from .models import Beekeeping, Location, Hive, Apiarist, Observation, LaborProcess, Treatment, Feeding, Harvest, NewsFeed

admin.site.register(Beekeeping)
admin.site.register(Location)
admin.site.register(Hive)
admin.site.register(Apiarist)

admin.site.register(Observation)
admin.site.register(LaborProcess)
admin.site.register(Treatment)
admin.site.register(Feeding)
admin.site.register(Harvest)

admin.site.register(NewsFeed)