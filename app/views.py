from django.contrib.auth.models import User
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required, user_passes_test
from django.http import HttpResponse
from .forms import (
                   NewLocationForm, NewHiveForm, NewObservationForm, NewLaborProcessForm, NewTreatmentForm, NewFeedingForm, NewHarvestForm, 
                   ObservationForm, LaborProcessForm, TreatmentForm, FeedingForm, HarvestForm, EditHiveForm, EditHiveNameForm
                   )
from .models import Beekeeping, Location, Hive, Observation, LaborProcess, Treatment, Feeding, Harvest, NewsFeed

@login_required
def index(request):
    user = request.user
    if not user.is_superuser:
        if user.apiarist.beekeeping:
            beekeeping = user.apiarist.beekeeping
            all_hives = beekeeping.all_hives.all()
            all_locations = beekeeping.locations.all()
            actions = NewsFeed.objects.all().filter(beekeeping=beekeeping).order_by('-executed_at')
            return render(request, 'index.html', {'beekeeping': beekeeping, 'all_hives': all_hives, 'actions': actions, 'all_locations' : all_locations})
        else:
            return HttpResponse("Du wurdest keiner Imkerei zugewiesen, bitte kontaktiere den Admin :)")
    else: 
        return redirect('admin:index')

@login_required
def index_locationfilter(request, locationname):
    user = request.user
    if not user.is_superuser:
        if user.apiarist.beekeeping:
            beekeeping = user.apiarist.beekeeping
            locations = Location.objects.all().filter(beekeeping=beekeeping)
            location = get_object_or_404(locations, name=locationname)
            all_hives = Hive.objects.all().filter(beekeeping=beekeeping, location=location)
            all_locations = beekeeping.locations.all()
            actions = NewsFeed.objects.all().filter(beekeeping=beekeeping).order_by('-executed_at')
            return render(request, 'index.html', {'beekeeping': beekeeping, 'all_hives': all_hives, 'actions': actions, 'all_locations' : all_locations})
        else:
            return HttpResponse("Du wurdest keiner Imkerei zugewiesen, bitte kontaktiere den Admin :)")
    else: 
        return redirect('admin:index')

@login_required
def my_account(request):
    return render(request, 'my_account.html')

@login_required
def hive(request, name):
    hive = get_object_or_404(Hive, name=name)
    actions = NewsFeed.objects.all().filter(hive=hive).order_by('-executed_at')
    pageUrl = "localhost:8000" + request.path
    return render(request, 'hive.html', {'hive': hive, 'actions' : actions, 'pageUrl': pageUrl})

@login_required
def delete_newsfeed(request, type, id):
    if type == "Beobachtung":
        Observation.objects.get(pk=id).delete()
        NewsFeed.objects.filter(action_type=type, action_id=id).delete()
    if type == "Arbeitsvorgang":
        LaborProcess.objects.get(pk=id).delete()
        NewsFeed.objects.filter(action_type=type, action_id=id).delete()
    if type == "Behandlung":
        Treatment.objects.get(pk=id).delete()
        NewsFeed.objects.filter(action_type=type, action_id=id).delete()
    if type == "Fütterung":
        Feeding.objects.get(pk=id).delete()
        NewsFeed.objects.filter(action_type=type, action_id=id).delete()
    if type == "Ernte":
        Harvest.objects.get(pk=id).delete()
        NewsFeed.objects.filter(action_type=type, action_id=id).delete()

    return redirect('index')

@login_required
def delete_action(request, type, id):
    if type == "Beobachtung":
        observation = Observation.objects.get(pk=id)
        hive = observation.hive
        observation.delete()
        NewsFeed.objects.filter(action_type=type, action_id=id).delete()
    if type == "Arbeitsvorgang":
        laborProcess = LaborProcess.objects.get(pk=id)
        hive = laborProcess.hive
        laborProcess.delete()
        NewsFeed.objects.filter(action_type=type, action_id=id).delete()
    if type == "Behandlung":
        treatment = Treatment.objects.get(pk=id)
        hive = treatment.hive
        treatment.delete()
        NewsFeed.objects.filter(action_type=type, action_id=id).delete()
    if type == "Fütterung":
        feeding = Feeding.objects.get(pk=id)
        hive = feeding.hive
        feeding.delete()
        NewsFeed.objects.filter(action_type=type, action_id=id).delete()
    if type == "Ernte":
        harvest = Harvest.objects.get(pk=id)
        hive = harvest.hive
        harvest.delete()
        NewsFeed.objects.filter(action_type=type, action_id=id).delete()

    return redirect('hive' , name=hive.name) 

def delete_hive(request, name):
    Hive.objects.filter(name=name).delete()
    
    return redirect('index')

def delete_location(request, pk):
    Location.objects.filter(pk=pk).delete()
    
    return redirect('location_management')

@login_required
def new_hive(request):
    user = request.user
    beekeeping = user.apiarist.beekeeping
    if request.method == 'POST':
        form = NewHiveForm(request.POST)
        if form.is_valid():
            hive = form.save(commit=False)
            hive.beekeeping = beekeeping
            hive.save()
            
            return redirect('index')  
    else:
        form = NewHiveForm()
    return render(request, 'new_hive.html', {'beekeeping': beekeeping, 'form': form})

@login_required
def new_location(request):
    user = request.user
    beekeeping = user.apiarist.beekeeping
    if request.method == 'POST':
        form = NewLocationForm(request.POST)
        if form.is_valid():
            location = form.save(commit=False)
            location.beekeeping = beekeeping
            location.save()
            
            return redirect('index')  
    else:
        form = NewLocationForm()
    return render(request, 'new_location.html', {'beekeeping': beekeeping, 'form': form})

@login_required
def new_observation(request, name):
    user = request.user
    hive = get_object_or_404(Hive, name=name)
    beekeeping = user.apiarist.beekeeping
    if request.method == 'POST':
        form = NewObservationForm(request.POST)
        if form.is_valid():
            observation = form.save(commit=False)
            observation.hive = hive
            observation.executed_by = user
            observation.save()            
            newsFeed = NewsFeed()
            newsFeed.action_type = observation.name
            newsFeed.action_id = observation.pk
            newsFeed.notes = observation.notes
            newsFeed.executed_at = observation.executed_at
            newsFeed.executed_by = observation.executed_by
            newsFeed.hive = hive
            newsFeed.beekeeping = beekeeping
            newsFeed.save()
            
            return redirect('hive', hive.name)  
    else:
        form = NewObservationForm()
    return render(request, 'new_observation.html', {'beekeeping': beekeeping, 'hive': hive, 'form': form})

@login_required
def new_laborprocess(request, name):
    user = request.user
    hive = get_object_or_404(Hive, name=name)
    beekeeping = user.apiarist.beekeeping
    if request.method == 'POST':
        form = NewLaborProcessForm(request.POST)
        if form.is_valid():
            laborprocess = form.save(commit=False)
            hive.droneFrame += laborprocess.droneFrame
            hive.brutFrame += laborprocess.brutFrame
            hive.emptyFrame += laborprocess.emptyFrame
            hive.feedFrame += laborprocess.feedFrame
            hive.mediumWallFrame += laborprocess.mediumWallFrame
            hive.honeyFrame += laborprocess.honeyFrame
            hive.emptyBox += laborprocess.emptyBox
            hive.beesEscape = laborprocess.beesEscape
            hive.fence = laborprocess.fence
            hive.diaper = laborprocess.diaper
            hive.flightHoleWedge = laborprocess.flightHoleWedge
            hive.save()
            laborprocess.hive = hive
            laborprocess.executed_by = user
            laborprocess.save()
            newsFeed = NewsFeed()
            newsFeed.action_type = laborprocess.name
            newsFeed.action_id = laborprocess.pk
            newsFeed.notes = laborprocess.notes
            newsFeed.executed_at = laborprocess.executed_at
            newsFeed.executed_by = laborprocess.executed_by
            newsFeed.hive = hive
            newsFeed.beekeeping = beekeeping
            newsFeed.save()

            
            return redirect('hive', hive.name)  
    else:
        form = NewLaborProcessForm()
    return render(request, 'new_laborprocess.html', {'beekeeping': beekeeping, 'hive': hive, 'form': form})

@login_required
def new_treatment(request, name):
    user = request.user
    hive = get_object_or_404(Hive, name=name)
    beekeeping = user.apiarist.beekeeping
    if request.method == 'POST':
        form = NewTreatmentForm(request.POST)
        if form.is_valid():
            treatment = form.save(commit=False)
            treatment.hive = hive
            treatment.executed_by = user
            treatment.save()
            newsFeed = NewsFeed()
            newsFeed.action_type = treatment.name
            newsFeed.action_id = treatment.pk
            newsFeed.notes = treatment.notes
            newsFeed.executed_at = treatment.executed_at
            newsFeed.executed_by = treatment.executed_by
            newsFeed.hive = hive
            newsFeed.beekeeping = beekeeping
            newsFeed.save()
            
            return redirect('hive', hive.name)  
    else:
        form = NewTreatmentForm()
    return render(request, 'new_treatment.html', {'beekeeping': beekeeping, 'hive': hive, 'form': form})

@login_required
def new_feeding(request, name):
    user = request.user
    hive = get_object_or_404(Hive, name=name)
    beekeeping = user.apiarist.beekeeping
    if request.method == 'POST':
        form = NewFeedingForm(request.POST)
        if form.is_valid():
            feeding = form.save(commit=False)
            feeding.hive = hive
            feeding.executed_by = user
            feeding.save()
            newsFeed = NewsFeed()
            newsFeed.action_type = feeding.name
            newsFeed.action_id = feeding.pk
            newsFeed.notes = feeding.notes
            newsFeed.executed_at = feeding.executed_at
            newsFeed.executed_by = feeding.executed_by
            newsFeed.hive = hive
            newsFeed.beekeeping = beekeeping
            newsFeed.save()
            
            return redirect('hive', hive.name)  
    else:
        form = NewFeedingForm()
    return render(request, 'new_feeding.html', {'beekeeping': beekeeping, 'hive': hive, 'form': form})

@login_required
def new_harvest(request, name):
    user = request.user
    hive = get_object_or_404(Hive, name=name)
    beekeeping = user.apiarist.beekeeping
    if request.method == 'POST':
        form = NewHarvestForm(request.POST)
        if form.is_valid():
            harvest = form.save(commit=False)
            harvest.hive = hive
            harvest.executed_by = user
            harvest.save()
            newsFeed = NewsFeed()
            newsFeed.action_type = harvest.name
            newsFeed.action_id = harvest.pk
            newsFeed.notes = harvest.notes
            newsFeed.executed_at = harvest.executed_at
            newsFeed.executed_by = harvest.executed_by
            newsFeed.hive = hive
            newsFeed.beekeeping = beekeeping
            newsFeed.save()
            
            return redirect('hive', hive.name)  
    else:
        form = NewHarvestForm()
    return render(request, 'new_harvest.html', {'beekeeping': beekeeping, 'hive': hive, 'form': form})

@login_required
def edit_hive(request, name):
    user = request.user
    hive = get_object_or_404(Hive, name=name)
    beekeeping = user.apiarist.beekeeping
    if request.method == 'POST':
        form = EditHiveForm(request.POST)
        if form.is_valid():
            hiveEdit = form.save(commit=False)
            hive.hiveType = hiveEdit.hiveType
            hive.beeQueenNr = hiveEdit.beeQueenNr
            hive.beeQueenBirthDate = hiveEdit.beeQueenBirthDate
            hive.beeQueenMarked = hiveEdit.beeQueenMarked
            hive.gentleness = hiveEdit.gentleness
            hive.honeyCombFaithful = hiveEdit.honeyCombFaithful
            hive.swarmInertia = hiveEdit.swarmInertia
            hive.stingingLoss = hiveEdit.stingingLoss
            hive.location = hiveEdit.location
            hive.notes = hiveEdit.notes
            hive.save()
      
    else:
        form = EditHiveForm(instance=hive)
    return render(request, 'edit_hive.html', {'beekeeping': beekeeping, 'hive': hive, 'form': form})

@login_required
def edit_hive_name(request, name):
    user = request.user
    hive = get_object_or_404(Hive, name=name)
    beekeeping = user.apiarist.beekeeping
    if request.method == 'POST':
        form = EditHiveNameForm(request.POST)
        if form.is_valid():
            hiveEdit = form.save(commit=False)
            hive.name = hiveEdit.name
            hive.save()

            return redirect('hive', hive.name)  
    else:
        form = EditHiveNameForm(instance=hive)
        return render(request, 'edit_hive_name.html', {'beekeeping': beekeeping, 'hive': hive, 'form': form})

@login_required
def observation(request, name, pk):
    user = request.user
    hive = get_object_or_404(Hive, name=name)
    beekeeping = user.apiarist.beekeeping
    observation = Observation.objects.get(id=pk)
    if request.method == 'GET':
        form = ObservationForm(instance=observation)
    else:
        form = ObservationForm()
    return render(request, 'observation.html', {'beekeeping': beekeeping, 'hive': hive, 'form': form, 'observation': observation})

@login_required
def laborProcess(request, name, pk):
    user = request.user
    hive = get_object_or_404(Hive, name=name)
    beekeeping = user.apiarist.beekeeping
    laborProcess = LaborProcess.objects.get(id=pk)
    if request.method == 'GET':
        form = LaborProcessForm(instance=laborProcess)
    else:
        form = LaborProcessForm()
    return render(request, 'laborProcess.html', {'beekeeping': beekeeping, 'hive': hive, 'form': form, 'laborProcess': laborProcess})

@login_required
def treatment(request, name, pk):
    user = request.user
    hive = get_object_or_404(Hive, name=name)
    beekeeping = user.apiarist.beekeeping
    treatment = Treatment.objects.get(id=pk)
    if request.method == 'GET':
        form = TreatmentForm(instance=treatment)
    else:
        form = TreatmentForm()
    return render(request, 'treatment.html', {'beekeeping': beekeeping, 'hive': hive, 'form': form, 'treatment': treatment})

@login_required
def feeding(request, name, pk):
    user = request.user
    hive = get_object_or_404(Hive, name=name)
    beekeeping = user.apiarist.beekeeping
    feeding = Feeding.objects.get(id=pk)
    if request.method == 'GET':
        form = FeedingForm(instance=feeding)
    else:
        form = FeedingForm()
    return render(request, 'feeding.html', {'beekeeping': beekeeping, 'hive': hive, 'form': form, 'feeding': feeding})

@login_required
def harvest(request, name, pk):
    user = request.user
    hive = get_object_or_404(Hive, name=name)
    beekeeping = user.apiarist.beekeeping
    harvest = Harvest.objects.get(id=pk)
    if request.method == 'GET':
        form = HarvestForm(instance=harvest)
    else:
        form = HarvestForm()
    return render(request, 'harvest.html', {'beekeeping': beekeeping, 'hive': hive, 'form': form, 'harvest': harvest})

@login_required
def location_management(request):
    beekeeping = request.user.apiarist.beekeeping
    return render(request, 'locations.html', {'beekeeping': beekeeping})