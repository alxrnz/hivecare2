from django.contrib.auth.models import User
from django.urls import reverse, resolve
from django.test import TestCase
from .views import home, beekeeping_locations, new_location
from .models import Beekeeping, Location

class HomeTests(TestCase):
    def setUp(self):
        self.beekeeping = Beekeeping.objects.create(name='Beekeeping', description='Esslingen Imkerei')
        url = reverse('home')
        self.response = self.client.get(url)

    def test_home_view_status_code(self):
        self.assertEquals(self.response.status_code, 200)

    def test_home_url_resolves_home_view(self):
        view = resolve('/')
        self.assertEquals(view.func, home)

    def test_home_view_contains_link_to_topics_page(self):
        beekeeping_locations_url = reverse('beekeeping_locations', kwargs={'pk': self.beekeeping.pk})
        self.assertContains(self.response, 'href="{0}"'.format(beekeeping_locations_url))

class BeekeepingLocationsTests(TestCase):
    def setUp(self):
        Beekeeping.objects.create(name='Imkerei', description='Esslingen Imkerei.')

    def test_beekeeping_locations_view_success_status_code(self):
        url = reverse('beekeeping_locations', kwargs={'pk': 1})
        response = self.client.get(url)
        self.assertEquals(response.status_code, 200)

    def test_beekeeping_locations_view_not_found_status_code(self):
        url = reverse('beekeeping_locations', kwargs={'pk': 99})
        response = self.client.get(url)
        self.assertEquals(response.status_code, 404)

    def test_beekeeping_locations_url_resolves_beekeeping_locations_view(self):
        view = resolve('/beekeepings/1/')
        self.assertEquals(view.func, beekeeping_locations)

    def test_beekeeping_locations_view_contains_navigation_links(self):
        beekeeping_locations_url = reverse('beekeeping_locations', kwargs={'pk': 1})
        homepage_url = reverse('home')
        new_location_url = reverse('new_location', kwargs={'pk': 1})

        response = self.client.get(beekeeping_locations_url)

        self.assertContains(response, 'href="{0}"'.format(homepage_url))
        self.assertContains(response, 'href="{0}"'.format(new_location_url))

class NewLocationTests(TestCase):
    def setUp(self):
        Beekeeping.objects.create(name='BeBee', description='BeBee bla.')
        User.objects.create_user(username='john', email='john@doe.com', password='123')

    def test_new_location_view_success_status_code(self):
        url = reverse('new_location', kwargs={'pk': 1})
        response = self.client.get(url)
        self.assertEquals(response.status_code, 200)

    def test_new_location_view_not_found_status_code(self):
        url = reverse('new_location', kwargs={'pk': 99})
        response = self.client.get(url)
        self.assertEquals(response.status_code, 404)

    def test_new_location_url_resolves_new_topic_view(self):
        view = resolve('/beekeepings/1/new/')
        self.assertEquals(view.func, new_location)

    def test_new_location_view_contains_link_back_to_beekeeping_locations_view(self):
        new_location_url = reverse('new_location', kwargs={'pk': 1})
        beekeeping_locations_url = reverse('beekeeping_locations', kwargs={'pk': 1})
        response = self.client.get(new_location_url)
        self.assertContains(response, 'href="{0}"'.format(beekeeping_locations_url))

    def test_csrf(self):
        url = reverse('new_location', kwargs={'pk': 1})
        response = self.client.get(url)
        self.assertContains(response, 'csrfmiddlewaretoken')

    def test_new_location_valid_post_data(self):
        url = reverse('new_location', kwargs={'pk': 1})
        data = {
            'address': 'Test address',
            'description': 'this is python'
        }
        response = self.client.post(url, data)
        self.assertTrue(Location.objects.exists())

    def test_new_location_invalid_post_data(self):
        url = reverse('new_location', kwargs={'pk': 1})
        response = self.client.post(url, {})
        self.assertEquals(response.status_code, 200)

    def test_new_location_invalid_post_data_empty_fields(self):
        url = reverse('new_location', kwargs={'pk': 1})
        data = {
            'address': '',
            'description': ''
        }
        response = self.client.post(url, data)
        self.assertEquals(response.status_code, 200)
        self.assertFalse(Location.objects.exists())