from django.urls import path
from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth import views as auth_views


from app import views
from accounts import views as accounts_views

urlpatterns = [
    #administrative
    path('admin', admin.site.urls, name='admin'),
    path('login', auth_views.LoginView.as_view(template_name='login.html'), name='login'),
    path('', views.index, name='index'),
    path('locationfilter/<str:locationname>', views.index_locationfilter, name='index_locationfilter'),
    path('logout', auth_views.LogoutView.as_view(), name='logout'),
    #user management
    path('my_account', views.my_account, name='my_account'),
    path('my_account/password', auth_views.PasswordChangeView.as_view(template_name='password_change.html'), name='password_change'),
    path('my_account/password/done', auth_views.PasswordChangeDoneView.as_view(template_name='password_change_done.html'), name='password_change_done'),

    #passwort reset
    path('reset', auth_views.PasswordResetView.as_view( template_name='password_reset.html',
                                                        email_template_name='password_reset_email.html',
                                                        subject_template_name='password_reset_subject.txt'),
                                                        name='password_reset'),
    path('reset/done', auth_views.PasswordResetDoneView.as_view(template_name='password_reset_done.html'), name='password_reset_done'),
    path('reset/<slug:uidb64>/<slug:token>/',
          auth_views.PasswordResetConfirmView.as_view(template_name='password_reset_confirm.html'),
          name='password_reset_confirm'),
    path('reset/complete', auth_views.PasswordResetCompleteView.as_view(template_name='password_reset_complete.html'), name='password_reset_complete'),

    #application pages    
    path('location_management', views.location_management, name='location_management'),
    path('new_hive', views.new_hive, name='new_hive'),    
    path('new_location', views.new_location, name='new_location'),
    path('edit_hive/<str:name>', views.edit_hive, name='edit_hive'),
    path('edit_hive_name/<str:name>', views.edit_hive_name, name='edit_hive_name'),
    path('delete_newsfeed/<str:type>/<int:id>', views.delete_newsfeed, name='delete_newsfeed'),
    path('delete_action/<str:type>/<int:id>', views.delete_action, name='delete_action'),
    path('delete_hive/<str:name>', views.delete_hive, name='delete_hive'),
    path('delete_location/<int:pk>', views.delete_location, name='delete_location'),
    path('<str:name>', views.hive, name='hive'),
    path('<str:name>/observation/<int:pk>', views.observation, name='observation'),
    path('<str:name>/laborProcess/<int:pk>', views.laborProcess, name='laborProcess'),
    path('<str:name>/treatment/<int:pk>', views.treatment, name='treatment'),
    path('<str:name>/feeding/<int:pk>', views.feeding, name='feeding'),
    path('<str:name>/harvest<int:pk>', views.harvest, name='harvest'),
    path('<str:name>/new_observation', views.new_observation, name='new_observation'),
    path('<str:name>/new_laborprocess', views.new_laborprocess, name='new_laborprocess'),
    path('<str:name>/new_treatment', views.new_treatment, name='new_treatment'),
    path('<str:name>/new_feeding', views.new_feeding, name='new_feeding'),
    path('<str:name>/new_harvest', views.new_harvest, name='new_harvest'),
]